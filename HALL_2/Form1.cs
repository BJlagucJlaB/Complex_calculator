﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet;
using System.Numerics;
namespace HALL_2
{
    public partial class Form1 : Form
    {

        Complexnumber A, B;
        public Form1()
        {
           
            InitializeComponent();
            comboBox1.Items.Add("число А");
            comboBox1.Items.Add("число B");

            comboBox3.Items.Add("число А");
            comboBox3.Items.Add("число B");

            comboBox2.Items.Add("+");
            comboBox2.Items.Add("-");
            comboBox2.Items.Add("/");
            comboBox2.Items.Add("*");
            comboBox2.Items.Add("=");
            comboBox2.Items.Add("^");
            comboBox2.Items.Add("√");



        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text == "√" || comboBox2.Text == "^")
            {
                label2.Text = "N";
                textBox4.Enabled = false;
            }
            else
            {
                label2.Text = "число B";
                textBox4.Enabled = true;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox5.Text = "";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {
            double newreal1, newimaginary1, newreal2, newimaginary2;
            if (double.TryParse(textBox1.Text,out newreal1)&&double.TryParse(textBox3.Text,out newimaginary1)&& double.TryParse(textBox2.Text, out newreal2) && double.TryParse(textBox4.Text, out newimaginary2))
            {
                A = new Complexnumber(newreal1, newimaginary1);
                B = new Complexnumber(newreal2, newimaginary2);
                
                textBox5.Clear();
                if (comboBox2.SelectedItem.ToString() == "+"){
                    A = A.Sum(A, B);
                    textBox5.Text += A.getreal()+" ";
                    textBox5.Text += A.getimaginary();
                }
                else if (comboBox2.SelectedItem.ToString() == "-"){
                    A = A.Sub(A, B);
                    textBox5.Text += A.getreal()+" ";
                    textBox5.Text += A.getimaginary();
                }
                else if (comboBox2.SelectedItem.ToString() == "="){
                    bool b = A.Equal(A, B);
                    if(b == true){
                        textBox5.Text += "Числа равны";
                    }else{
                        textBox5.Text += "Числа не равны";
                    }
                    
                }
            }
        }
    }
}

public class Complexnumber
{
    public Complexnumber(double newreal,double newimaginary) 
        {
        real = newreal;
        imaginary = newimaginary;
        }
    double real,imaginary;
    public double getreal()
    {
        return real;
    }
    public double getimaginary()
    {
        return imaginary;
    }
    public void setreal(double newreal)
    {
        real = newreal;
    }
    public void setimaginary(double newimaginary)
    {
        imaginary = newimaginary;
    }

    public Complexnumber Sum(Complexnumber A, Complexnumber B){
        Complexnumber C = new Complexnumber(0, 0);
        if (A != null && B != null){
            C.real = A.real + B.real;
            C.imaginary = A.imaginary + B.imaginary;
        }
        return C;
    }

    public Complexnumber Sub(Complexnumber A, Complexnumber B){
        Complexnumber C = new Complexnumber(0, 0);
        if (A != null && B != null){
            C.real = A.real - B.real;
            C.imaginary = A.imaginary - B.imaginary;
        }
        return C;
    }

    public bool Equal(Complexnumber A, Complexnumber B){
        if (A != null && B != null){
            if(A.real == B.real && A.imaginary == B.imaginary){
                return true;
            }                  
        }
        return false;
    }
}